import type { NextPage } from 'next'
import Head from 'next/head'
import { Counter } from '../components/counter'
import { AppSettings, Options } from '../components/options'
import { useLocalStorage } from '../components/useLocalStorage'
import { useKeyPress } from '../components/useKeyPress';
import styles from '../styles/Home.module.css'
import { useEffect, useState } from 'react'

const Home: NextPage = () => {
  
  const [appSettings, setAppSettings] = useLocalStorage<AppSettings>("appSettings",{pattern: [1,2,3], baseHue: 320});
  const [currentIndex, setCurrentIndex] = useLocalStorage<number>("currentIndex", 0);
  const [nextPressed, setNextPressed] = useState(false);
  const [backPressed, setBackPressed] = useState(false);

  const [touchStart, setTouchStart] = useState<number | null>(null)
  const [touchEnd, setTouchEnd] = useState<number | null>(null)

  // the required distance between touchStart and touchEnd to be detected as a swipe
  const minSwipeDistance = 50 

  const onTouchStart = (e: React.TouchEvent<HTMLElement>) => {
    setTouchEnd(null) // otherwise the swipe is fired even with usual touch events
    setTouchStart(e.targetTouches[0].clientX)
  }

  const onTouchMove = (e: React.TouchEvent<HTMLElement>) => setTouchEnd(e.targetTouches[0].clientX)

  const onTouchEnd = () => {
    if ((!touchStart || !touchEnd)) {
      return
    }
    const distance = touchStart - touchEnd;
    const isLeftSwipe = distance > minSwipeDistance
    const isRightSwipe = distance < -minSwipeDistance
    if (isLeftSwipe) {
      onUpdateRequested(decreaseIndex);
    } else if (isRightSwipe) {
      onUpdateRequested(increaseIndex);
    }
  }

  const increaseIndex = (next: number) => {
      if (next + 1 > appSettings.pattern.length -1 ) {
        setCurrentIndex(0);
        return;
      }
      setCurrentIndex(next + 1);
  }

  const decreaseIndex = (next: number) => {
    if (next - 1 < 0) {
      setCurrentIndex(appSettings.pattern.length);
      return;
    }
    setCurrentIndex(next - 1);
  }

  const onUpdateRequested =  (method: (next: number) => void) => {
    method(currentIndex);
    if (window?.getSelection) {
      window?.getSelection()?.removeAllRanges();
    }
  }

  const onOptionsUpdated = (options: AppSettings) => {
    setAppSettings(options);
  }

  useKeyPress(()=>{
    setNextPressed(true)
  }, ["Space", "ArrowRight"], );

  useKeyPress(()=>{
    setBackPressed(true)
  }, ["ArrowLeft"], );

  if(nextPressed) {
    setNextPressed(false);
    onUpdateRequested(increaseIndex);
  }

  if(backPressed) {
    setBackPressed(false);
    onUpdateRequested(decreaseIndex);
  }
  
  return (
    <div className={styles.container}>
      <Head>
        <title>Pocitadlo</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}  onTouchStart={onTouchStart} onTouchMove={onTouchMove} onTouchEnd={onTouchEnd} onClick={(e)=>{
        onUpdateRequested(increaseIndex);
        e.stopPropagation();
      }}>
        <Counter selectedIndex={currentIndex} baseHue={appSettings.baseHue} pattern={appSettings.pattern} />
        <Options onOptionsUpdated={onOptionsUpdated} defaultSettings={appSettings}/>
      </main>
    </div>
  )
}

export default Home;

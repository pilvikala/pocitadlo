import styles from "./counterElement.module.css";

interface CounterElementProps {
    label: string;
    selected?: boolean;
    color? : string;
}

export const CounterElement = ({label, selected, color}: CounterElementProps) => {
    const selectedLeft = selected ? "▶" : "";
    const selectedRight = selected ? "◀" : "";
    const style = selected ? styles.selected : "";
    return <div className={`${styles.counterElement} ${style}`} style={{color: color || ""}} >{selectedLeft}{label}{selectedRight}</div>;
};

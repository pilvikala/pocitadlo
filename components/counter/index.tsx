import styles from "./index.module.css";
import { useState } from "react";
import { CounterElement } from "./counterElement";
import memoizeOne from "memoize-one";
import { getColors } from "../colors";

interface CounterProps {
  pattern: number[];
  baseHue: number;
  selectedIndex: number;
}

const getUnique = (pattern: number[]): number[] => {
  const unique: number[] = []
  pattern.forEach((n)=> {
    if (!unique.includes(n)) unique.push(n)
  })
  return unique;
} 

interface ColorMap {
  [key: number]: string;
}

const getColorsForPattern = (pattern: number[], baseHue: number): ColorMap  => {
  const keys = getUnique(pattern);
  const colors =  getColors(keys.length, baseHue);
  const map: ColorMap = {};
  keys.forEach((k, index) => {
    map[k] = colors[index]
  });
  return map;
}

export const Counter = ({pattern, baseHue, selectedIndex}: CounterProps) => {
    
    const memoizedGetColors = memoizeOne(getColorsForPattern);
    const colors = memoizedGetColors(pattern, baseHue);


    return (
      <div className={styles.counter}>
        {
          pattern.map((n, idx) => (
             <CounterElement key={idx} label={n.toString()} selected={idx === selectedIndex} color={colors[n]}/>
          ))
        }
      </div>);
}
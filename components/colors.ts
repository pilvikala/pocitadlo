function HSLToRGB(h: number, s: number, l: number): string {
    // Must be fractions of 1
    s /= 100;
    l /= 100;
  
    let c = (1 - Math.abs(2 * l - 1)) * s,
        x = c * (1 - Math.abs((h / 60) % 2 - 1)),
        m = l - c/2,
        r = 0,
        g = 0,
        b = 0;
        
    if (0 <= h && h < 60) {
        r = c; g = x; b = 0;  
    } else if (60 <= h && h < 120) {
        r = x; g = c; b = 0;
    } else if (120 <= h && h < 180) {
        r = 0; g = c; b = x;
    } else if (180 <= h && h < 240) {
        r = 0; g = x; b = c;
    } else if (240 <= h && h < 300) {
        r = x; g = 0; b = c;
    } else if (300 <= h && h < 360) {
        r = c; g = 0; b = x;
    }
    r = Math.round((r + m) * 255);
    g = Math.round((g + m) * 255);
    b = Math.round((b + m) * 255);

    return "rgb(" + r + "," + g + "," + b + ")";
  }

const baseSaturation = 66;

export const getColor = (baseHue: number): string => {
    return HSLToRGB(baseHue, 66, 50);
}

export const getColors = (count: number, baseHue?: number): string[] => {
    const defaultBaseHue = 320;
    const minLuminance = 20;
    const maxLuminance = 80;

    let hue = baseHue ? baseHue : defaultBaseHue;

    if(count > 12) return ["rgba(0,0,0,1)"];

    if(count > 6) return ["rgba(0,0,0,1)"];

    const luminanceStep = (maxLuminance - minLuminance) / 6;

    const colors: string[] = [];
    for(let l = minLuminance; l <= maxLuminance; l+=luminanceStep ) {
        colors.push(HSLToRGB(hue, baseSaturation, l));
    }
    return colors;
}
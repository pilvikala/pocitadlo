import { useState } from "react";
import { getColor } from "../colors";
import styles from "./index.module.css";

export interface AppSettings {
    pattern: number[],
    baseHue: number,
}

interface OptionsProps {
    onOptionsUpdated: (newSettings: AppSettings) => void;
    defaultSettings: AppSettings;
}

const patternToString = (pattern: number[]): string => {
    return pattern.map((c)=>c.toString()).join("");
}

export const Options = ({onOptionsUpdated, defaultSettings}: OptionsProps) => {
    const saveOptions = (patternValue: string) => {
        let pattern = patternValue ? patternValue.split('').map((c)=>{
            return parseInt(c, 10);
        }).filter((n)=>!isNaN(n)) : [];
        onOptionsUpdated({
              pattern,
              baseHue: hue
            }
        );
    }
    const [expanded, setExpanded] = useState(false);
    const [patternValue, setPatternValue] = useState(patternToString(defaultSettings.pattern));
    const [hue, setHue] = useState(defaultSettings.baseHue);
    const containerStyle = expanded ? `${styles.container} ${styles.containerExpanded}` : styles.container;
    const sliderColor = getColor(hue);
    return (
        <div className={containerStyle} onClick={() => {if (!expanded) setExpanded(true)}} >
            <div className={styles.text} >
                Options
            </div>
            {expanded && (<div className={styles.optionsContainer}>
                <div className={styles.flexContainer}>
                    <input className={styles.input} type="text" onChange={(event) => setPatternValue(event.target.value)} defaultValue={patternValue}></input>
                </div>
                <div className={styles.flexContainer}>
                    <div className={styles.slidecontainer}>
                        <input style={{background: sliderColor}} type="range" min="0" max="360" value={hue} className={styles.slider} onChange={(event) => {setHue(parseInt(event.target.value))}} />
                    </div>    
                </div>
                <div className={styles.flexContainer}>
                    <button
                        className={styles.button}
                        onClick={() => {
                        saveOptions(patternValue);
                        setExpanded(false)
                    }}>Save</button>
                    <button className={styles.button} onClick={() => {setExpanded(false); setPatternValue(patternToString(defaultSettings.pattern))}}>Cancel</button>
                </div>
            </div>)}
        </div>
    )
}